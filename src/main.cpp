#include <iostream>
#include <vector>

#include <Variant/Variant.h>
#include <Variant/Schema.h>
#include "rect.h"
#include "utils.h"

using libvariant::Variant;

const char kRects[] = "rects";
const char kSchemaPath[] = "data/rects_schema.json";

int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " <filename>\n";
        std::cout << "filename - path to the file to parse\n";
        return -1;
    }

    Variant rects_variant;
    try {
        rects_variant = libvariant::DeserializeJSONFile(argv[1]);
    } catch (std::runtime_error &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return -2;
    }

    Variant schema_variant;
    try {
        schema_variant = libvariant::DeserializeJSONFile(kSchemaPath);
    } catch (std::runtime_error &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return -2;
    }

    libvariant::SchemaResult validation_result = libvariant::SchemaValidate(schema_variant, rects_variant);
    if (validation_result.Error()) {
        std::cerr << "Error: " << validation_result;
        return -3;
    }

    std::cout << "Input:\n";
    std::vector<Rect> rects;
    Variant::List rects_array = rects_variant[kRects].AsList();
    for (size_t i = 0; i < rects_array.size(); ++i) {
        Rect rect(rects_array[i]["x"].AsInt(),
                  rects_array[i]["y"].AsInt(),
                  rects_array[i]["w"].AsInt(),
                  rects_array[i]["h"].AsInt());

        if (std::find (rects.begin(), rects.end(), rect) == rects.end()) {
            rects.push_back(rect);

            std::cout << "      " << rects.size() << ": Rectangle at ";
            utils::pretty_print_coords(rect);
            std::cout << "\n";
        }
    }

    std::cout << "\nOverlaps\n";
    utils::overlaps(rects);

    int64_t area = utils::area(rects);
    std::cout << "\nArea\n      " << area << "\n";

    return 0;
}
