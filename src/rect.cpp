#include "rect.h"

#include "algorithm"

Rect::Rect()
    : x_(0), y_(0), w_(0), h_(0)
{}

Rect::Rect(int x, int y, int w, int h)
    : x_(x), y_(y), w_(w), h_(h)
{}

bool Rect::valid() const
{
    return w_ > 0 && h_ > 0;
}

int Rect::x() const
{
    return x_;
}

int Rect::y() const
{
    return y_;
}

int Rect::w() const
{
    return w_;
}

int Rect::h() const
{
    return h_;
}

bool Rect::operator==(const Rect &other) const
{
    return x_ == other.x_ && y_ == other.y_ && w_ == other.w_ && h_ == other.h_;
}

Rect Rect::intersection(const Rect &other) const
{
    int x = std::max(x_, other.x_);
    int y = std::max(y_, other.y_);
    int w = std::min(x_ + w_, other.x_ + other.w_) - x;
    int h = std::min(y_ + h_, other.y_ + other.h_) - y;

    Rect result(x, y, w, h);
    if (result.valid())
        return result;
    else
        return Rect();
}

int Rect::area() const
{
    return w_ * h_;
}
