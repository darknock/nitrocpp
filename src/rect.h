#ifndef RECT_H
#define RECT_H

class Rect
{
public:
    Rect();
    Rect(int x, int y, int w, int h);
    Rect(const Rect &other) = default;

    bool valid() const;
    int x() const;
    int y() const;
    int w() const;
    int h() const;

    bool operator==(const Rect &other) const;
    Rect intersection(const Rect &other) const;
    int area() const;

private:
    int x_, y_, w_, h_;
};

#endif // RECT_H
