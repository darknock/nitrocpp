#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include "rect.h"

namespace utils {

std::vector<std::vector<int>> combinations (int n, int k)
{
    std::vector<std::vector<int> > result;
    if (n < k || k < 1)
        return result;

    std::vector<bool> mask(n);
    std::fill(mask.begin(), mask.begin() + k, true);

    do {
        std::vector<int> combination;
        for (int i = 0; i < n; i++) {
            if (mask[i]) {
                combination.push_back(i);
            }
        }
        result.push_back(std::move(combination));
    } while (std::prev_permutation(mask.begin(), mask.end()));

    return result;
}

void pretty_print_coords(const Rect &rect)
{
    std::cout << "(" << rect.x() << "," << rect.y()
              << "), w=" << rect.w() << ", h=" << rect.h()
              << ", area=" << rect.area() << ".";
}

void overlaps(const std::vector<Rect> &rects, bool distinct = true)
{
    std::vector<Rect> unique;
    for (size_t k = 2; k <= rects.size(); ++k) {
        // get all k-combinations
        std::vector<std::vector<int>> combos = combinations(rects.size(), k);
        for (auto combo : combos) {
            Rect rect(rects[combo[0]]);
            for (int i = 1; i < k; ++i) {
                rect = rect.intersection(rects[combo[i]]);
                if(!rect.valid())
                    break;
            }
            if (rect.valid() && (!distinct || (std::find(unique.begin(), unique.end(), rect) == unique.end()))) {
                unique.push_back(rect);

                std::cout << "      Between rectangle ";
                for (int i = 0; i < k; ++i) {
                    std::cout << combo[i] + 1;
                    if (i < k - 2) {
                        std::cout << ", ";
                    } else if (i < k - 1) {
                        std::cout << " and ";
                    }
                }
                std::cout << " at ";
                pretty_print_coords(rect);
                std::cout << "\n";
            }
        }
    }
}

int64_t area(const std::vector<Rect> &rects)
{
    uint64_t area = 0;
    for (size_t k = rects.size(); k > 0; --k) {
        uint64_t combo_area = 0;
        std::vector<std::vector<int>> combos = combinations(rects.size(), k);
        for (auto combo : combos) {
            Rect rect(rects[combo[0]]);
            for (int i = 1; i < k; ++i) {
                rect = rect.intersection(rects[combo[i]]);
                if(!rect.valid())
                    break;
            }
            combo_area += rect.area();
        }
        area = combo_area - area;
    }

    return area;
}

} // namespace utils

#endif // UTILS_H
