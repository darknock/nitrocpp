#include "catch.hpp"

#include "rect.h"

TEST_CASE( "rect", "[rect]" ) {
    Rect rect(100, 120, 250, 180);

    REQUIRE( rect.x() == 100 );
    REQUIRE( rect.y() == 120 );
    REQUIRE( rect.w() == 250 );
    REQUIRE( rect.h() == 180 );
}

TEST_CASE( "rect_valid", "[rect]" ) {
    Rect rect1(100, 100, 250, 80);
    Rect rect2(120, 200, 250, 150);
    Rect rect3(140, 160, 250, 100);

    SECTION("no") {
        Rect result = rect1.intersection(rect2);
        CHECK( result.x() == 0 );
        CHECK( result.y() == 0 );
        CHECK( result.w() == 0 );
        CHECK( result.h() == 0 );
        REQUIRE_FALSE( result.valid() );
    }

    SECTION("yes") {
        Rect result = rect1.intersection(rect3);
        CHECK( result.x() == 140 );
        CHECK( result.y() == 160 );
        CHECK( result.w() == 210 );
        CHECK( result.h() ==  20 );
        REQUIRE( result.valid() );
    }
}

TEST_CASE( "rect_equals", "[rect]" ) {
    Rect rect0(1, 2, 3, 4);
    Rect rect1(9, 2, 3, 4);
    Rect rect2(1, 9, 3, 4);
    Rect rect3(1, 2, 9, 4);
    Rect rect4(1, 2, 3, 9);

    CHECK( rect0 == rect0 );
    CHECK_FALSE( rect0 == rect1 );
    CHECK_FALSE( rect0 == rect2 );
    CHECK_FALSE( rect0 == rect3 );
    CHECK_FALSE( rect0 == rect4 );
}

TEST_CASE( "rect_intersection", "[rect]" ) {
    Rect rect1(100, 100, 250,  80);
    Rect rect2(140, 160, 250, 100);
    Rect rectX(140, 160, 210,  20);

    REQUIRE( rect1.intersection(rect2) == rectX );
}

TEST_CASE( "rect_area", "[rect]" ) {
    Rect rect0(  0,   0, 2,  4);
    Rect rect1(  0,   0, 5,  6);
    Rect rect2(100, 100, 6,  5);

    REQUIRE( rect0.area() == 8 );
    REQUIRE( rect1.area() == rect2.area() );
}
