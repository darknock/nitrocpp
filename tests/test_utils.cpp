#include "catch.hpp"

#include "utils.h"

TEST_CASE( "combinations", "[utils]" ) {
    std::vector<std::vector<int>> combos1 = utils::combinations(3, 2);
    REQUIRE( combos1.size() == 3 );
    REQUIRE( combos1.at(0).size() == 2 );
    REQUIRE( combos1.at(0) == std::vector<int>({ 0, 1 }) );
    REQUIRE( combos1.at(1) == std::vector<int>({ 0, 2 }) );
    REQUIRE( combos1.at(2) == std::vector<int>({ 1, 2 }) );

    std::vector<std::vector<int>> combos2 = utils::combinations(3, 3);
    REQUIRE( combos2.size() == 1 );
    REQUIRE( combos2.at(0).size() == 3 );
    REQUIRE( combos2.at(0) == std::vector<int>({ 0, 1, 2 }) );

    std::vector<std::vector<int>> combos3 = utils::combinations(3, 0);
    REQUIRE( combos3.size() == 0 );

    std::vector<std::vector<int>> combos4 = utils::combinations(2, 3);
    REQUIRE( combos4.size() == 0 );

    std::vector<std::vector<int>> combos5 = utils::combinations(0, 0);
    REQUIRE( combos4.size() == 0 );
}

TEST_CASE( "area", "[utils]" ) {
    std::vector<Rect> rects = {
        Rect (10, 10, 25,  8),
        Rect (14, 16, 25, 10),
        Rect (14, 16, 21,  2)
    };

    uint64_t area = utils::area(rects);
    REQUIRE( area == 408 );

    rects.push_back(Rect(13, 17, 21, 2));
    uint64_t area2 = utils::area(rects);
    REQUIRE( area2 == 409 );

    rects.push_back(Rect(35, 26, 10, 10));
    uint64_t area3 = utils::area(rects);
    REQUIRE( area3 == 509 );

    rects.push_back(Rect(36, 27, 8, 8));
    uint64_t area4 = utils::area(rects);
    REQUIRE( area4 == 509 );
}
